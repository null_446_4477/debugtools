#include "stm32f4xx.h"
#include "stm32f4xx_conf.h"

#include "sys_defines.h"
#include "usbd_cdc_vcp.h"

#include "dac.h"
#include "delay.h"

#define udelay(x) delay_us(x)
#define mdelay(x) delay_ms(x)

static int dac_en = 0;
static u16 dac_dr = 1600;

u16 dac_mv(u16 d)
{
	double v = d / 1000.0;
	d = (v / 3.3 * 4096) + 0.5;
	return d;
}

// pwm 模块
void dac_init(void)
{
	GPIO_InitTypeDef gpio = {0};
	
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);

	// pa5 dac_out2
	gpio.GPIO_Pin = GPIO_Pin_5;
	gpio.GPIO_Speed = GPIO_Speed_100MHz;
	gpio.GPIO_Mode = GPIO_Mode_AN;
	gpio.GPIO_OType = GPIO_OType_PP;
	gpio.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOA, &gpio);
}

int dac_out()
{
	DAC_SetChannel2Data(DAC_Align_12b_R, dac_mv(dac_dr));
	DAC_SoftwareTriggerCmd(DAC_Channel_2, ENABLE);
	return 0;
}

int dac_start()
{
	DAC_InitTypeDef DAC_InitStruct;
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_DAC, ENABLE);
	DAC_DeInit();
	memset(&DAC_InitStruct, 0, sizeof(DAC_InitStruct));
	/* Set DAC options */
	DAC_InitStruct.DAC_Trigger = DAC_Trigger_Software;
	DAC_InitStruct.DAC_WaveGeneration = DAC_WaveGeneration_None;
	DAC_InitStruct.DAC_OutputBuffer = DAC_OutputBuffer_Enable;
	
	/* Init and enable proper DAC */
	DAC_Init(DAC_Channel_2, &DAC_InitStruct);
	/* Enable DAC channel 2 */
	DAC_Cmd(DAC_Channel_2, ENABLE);

	dac_out();
	return 0;
}

int dac_stop()
{
	DAC_DeInit();
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_DAC, DISABLE);
	return 0;
}

// dac命令解析
void dac_data_parse(u8 *data)
{
//	u16 len;
//	len = (u16)((data[LENGTH] << 8) + data[LENGTH + 1]);
	switch (data[CMD]) {
		case 3:
			// 配置pwm
			dac_en = data[DATA];
			if (dac_en) {
				dac_start();
			} else {
				dac_stop();
			}
			break;
		case 4:
			memcpy(&dac_dr, data + DATA, 2);
			dac_out();
			break;
		default :
			break;
	}
	return;
}

