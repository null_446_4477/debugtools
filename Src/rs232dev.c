#include "stm32f4xx.h"
#include "rs232dev.h"
#include "kfifo.h"
#include "msg_pack.h"
#include "sys_defines.h"
#include "usbd_cdc_vcp.h"

/* rx/tx buffer, used by DMA */
static u8 dma_rxbuf[USART_REC_LEN];
static u8 dma_txbuf[USART_REC_LEN];

/* tx/rx fifo */
static struct kfifo tx;
static struct kfifo rx;
static u8 rx_buf[4096];
static u8 tx_buf[4096];

static u8 buf_pack[USART_REC_LEN + FRAME_REGULAR_LEN];//接收到的数据进行打包的buffer
static u8 buf_parse[USB_PC_SEND_LEN];//从USB收到的数据解析后的缓存

static rs232_param rs232 = {115200, 1, 8, 1};


void get_sys_clk(void)
{
	RCC_ClocksTypeDef sys_clk;
	RCC_GetClocksFreq(&sys_clk);
}

static int _fifo_init(void)
{
	if (kfifo_init(&rx, rx_buf, sizeof(rx_buf)) < 0) {
		printf("err: rx fifo init error\n");
		return -1;
	}
	if (kfifo_init(&tx, tx_buf, sizeof(tx_buf)) < 0) {
		printf("err: tx fifo init error\n");
		return -1;
	}
	return 0;
}
/** NVIC初始化 */
static void _nvic_init(FunctionalState state)
{
	NVIC_InitTypeDef nvic;
	
	// config dma nvic tx
	nvic.NVIC_IRQChannel = DMA2_Stream7_IRQn;
	nvic.NVIC_IRQChannelPreemptionPriority = 3;
	nvic.NVIC_IRQChannelSubPriority = 0;
	nvic.NVIC_IRQChannelCmd = state;
	NVIC_Init(&nvic);
	
	// config dma nvic rx
	nvic.NVIC_IRQChannel = DMA2_Stream2_IRQn;
	nvic.NVIC_IRQChannelPreemptionPriority = 0;
	nvic.NVIC_IRQChannelSubPriority = 0;
	nvic.NVIC_IRQChannelCmd = state;
	NVIC_Init(&nvic);

	// config uart2 nvic
	nvic.NVIC_IRQChannel = USART1_IRQn;
	nvic.NVIC_IRQChannelPreemptionPriority = 0;
	nvic.NVIC_IRQChannelSubPriority = 1;
	nvic.NVIC_IRQChannelCmd = state;
	NVIC_Init(&nvic);

	return;
}

/*
 * uart DMA发送
 */
static int _dma_tx(u8 *buf, u16 len)
{
	DMA_InitTypeDef dma;
	memset(&dma, 0, sizeof(DMA_InitTypeDef));
	// 设置DMA通道
	dma.DMA_Channel = DMA_Channel_4;
	// 设置寄存器地址
	dma.DMA_PeripheralBaseAddr = (u32)(&USART1->DR);
	// 设置内存地址
	dma.DMA_Memory0BaseAddr = (u32)(buf);
	// 设置传输方向
	dma.DMA_DIR = DMA_DIR_MemoryToPeripheral;
	// 设置缓冲长度
	dma.DMA_BufferSize = len;
	// 设置外设地址递增
	dma.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	// 设置内存地址递增
	dma.DMA_MemoryInc = DMA_MemoryInc_Enable;
	// 设置外设数据字长
	dma.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
	// 设置内存数据字长
	dma.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
	// DMA模式
	dma.DMA_Mode = DMA_Mode_Normal;
	// 设置优先级
	dma.DMA_Priority = DMA_Priority_Medium;
	// 设置是否使用FIFO
	dma.DMA_FIFOMode = DMA_FIFOMode_Disable;
	// 设置FIFO阈值
	dma.DMA_FIFOThreshold = DMA_FIFOThreshold_1QuarterFull;
	// 
	dma.DMA_MemoryBurst = DMA_MemoryBurst_Single;
	//
	dma.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
	// 初始化DMA
	DMA_Init(DMA2_Stream7, &dma);
	// 关闭双缓冲模式
	DMA_DoubleBufferModeCmd(DMA2_Stream7, DISABLE);
	// 设置DMA中断模式
	DMA_ITConfig(DMA2_Stream7, DMA_IT_TC, ENABLE);
	// 设置长度计数
	DMA_SetCurrDataCounter(DMA2_Stream7, len);
	// 打开DMA
	DMA_Cmd(DMA2_Stream7, ENABLE);
	return 0;
}

/*
 * UART DMA RX start
 */
static void _dma_rx()
{
	DMA_InitTypeDef dma;
	u16 len = sizeof(dma_rxbuf);
	memset(&dma, 0, sizeof(DMA_InitTypeDef));
	// 设置DMA通道	
	dma.DMA_Channel = DMA_Channel_4;
	// 设置寄存器地址
	dma.DMA_PeripheralBaseAddr = (u32)(&USART1->DR);
	// 设置内存地址
	dma.DMA_Memory0BaseAddr = (u32)(dma_rxbuf);
	// 设置传输方向
	dma.DMA_DIR = DMA_DIR_PeripheralToMemory;
	// 设置缓冲长度
	dma.DMA_BufferSize = len;
	// 设置外设地址递增
	dma.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	// 设置内存地址递增
	dma.DMA_MemoryInc = DMA_MemoryInc_Enable;
	// 设置外设数据字长
	dma.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
	// 设置内存数据字长
	dma.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
	// DMA模式
	dma.DMA_Mode = DMA_Mode_Normal;
	// 设置优先级
	dma.DMA_Priority = DMA_Priority_VeryHigh; //DMA_Priority_Medium
	// 设置是否使用FIFO
	dma.DMA_FIFOMode = DMA_FIFOMode_Disable;
	// 设置FIFO阈值
	dma.DMA_FIFOThreshold = DMA_FIFOThreshold_1QuarterFull;
	//
	dma.DMA_MemoryBurst = DMA_MemoryBurst_Single;
	//
	dma.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
	// 初始化DMA
	DMA_Init(DMA2_Stream2, &dma);
	// 关闭双缓冲模式
	DMA_DoubleBufferModeCmd(DMA2_Stream2, DISABLE);
	// 设置DMA中断模式
	DMA_ITConfig(DMA2_Stream2, DMA_IT_TC, ENABLE);		// complete
	// 设置长度计数
	DMA_SetCurrDataCounter(DMA2_Stream2, len);
	// 打开DMA
	DMA_Cmd(DMA2_Stream2, ENABLE);
	return;
}

void uart1_init(rs232_param *param)
{
	/* gpio config */
	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;

	_nvic_init(ENABLE);
	/* Enable the GPIOs clocks */
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
	/* Enable the UART1 clocks */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
	/* Enable the DMA2 clocks */
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA2, ENABLE);

	/* Configure uart1_tx as alternate function push-pull */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;	// wjzhe, 10M to 2M
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	/* Configure usat1_rx as input floating */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	GPIO_PinAFConfig(GPIOA, GPIO_PinSource9, GPIO_AF_USART1);
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource10, GPIO_AF_USART1);
	
	/* USART2 configured as follow:
		- BaudRate = 115200 baud
		- Word Length = 8 Bits
		- One Stop Bit
		- No parity
		- Hardware flow control disabled (RTS and CTS signals)
		- Receive and transmit enabled
	*/
	memset(&USART_InitStructure, 0, sizeof(USART_InitStructure));
	USART_InitStructure.USART_BaudRate = param->baud;;
	if (param->data_bit == 8) {
		USART_InitStructure.USART_WordLength = USART_WordLength_8b; 
	} else if (param->data_bit == 9) {
		USART_InitStructure.USART_WordLength = USART_WordLength_9b; 
	} else {
		USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	}
	if (param->stop_bit == 1) {
		USART_InitStructure.USART_StopBits = USART_StopBits_1; 
	} else if (param->stop_bit == 2) {
		USART_InitStructure.USART_StopBits = USART_StopBits_1_5; 
	} else if (param->stop_bit == 3) {
		USART_InitStructure.USART_StopBits = USART_StopBits_2; 
	}
	if (param->parity_bit == 1) {
		USART_InitStructure.USART_Parity = USART_Parity_No; 
	} else if (param->parity_bit == 2) {
		USART_InitStructure.USART_Parity = USART_Parity_Odd; 
	} else if (param->parity_bit == 3) {
		USART_InitStructure.USART_Parity = USART_Parity_Even; 
	}
	
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None; 
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx; 
	
	/* Configure the USART1*/ 
	USART_Init(USART1, &USART_InitStructure); 

	// Deinitializes the DMA1 Channel
	DMA_DeInit(DMA2_Stream2);
	DMA_DeInit(DMA2_Stream7);

	// DMA发送UART数据
	USART_DMACmd(USART1, USART_DMAReq_Tx, ENABLE);

	// DMA接收UART数据
	USART_DMACmd(USART1, USART_DMAReq_Rx, ENABLE);

	// 开启接收dma rx
	_dma_rx();

	/* Uart Interrupt config, enable idle intr */
	USART_ITConfig(USART1, USART_IT_IDLE, ENABLE);
	
	/* Enable the USART1 */
	USART_Cmd(USART1, ENABLE); 

	/* 初始化FIFO */
	_fifo_init();
}
void uart_init(void)
{
	uart1_init(&rs232);
}
static int _wan_write()
{
	u16 len;
	struct kfifo *fifo = &tx;
	
	// 发送FIFO未发送完成的数据
	len = kfifo_len(fifo);
	if (len) {
		len = min(len, sizeof(dma_txbuf));
		kfifo_get(fifo, dma_txbuf, len);
		_dma_tx(dma_txbuf, len);
	}
	return 0;
}
/*
 * 
 */
static __inline int wan_recv(int flag)
{
	u16 len = sizeof(dma_rxbuf);
	
	if (!flag) {
		// 计算数据长度
		len -= DMA2_Stream2->NDTR;
	}
	// 打包
	len = updata_send_pack(TYPE_RS232, CMD_RECIVE, dma_rxbuf, len, buf_pack);
	// 放入发送FIFO
	VCP_DataTx(buf_pack, len);
	return 0;
}
// 发送数据
static int wan_write(u8 *buf, u16 len)
{
	struct kfifo *fifo = &tx;
	
	if (kfifo_avail(fifo) < len) {
		printf("wan_write: warning gprs tx fifo full...%d, l = %d\n",
			kfifo_avail(fifo), len);
		return -1;
	}

	kfifo_put(fifo, buf, len);

	if (!(DMA2_Stream7->CR & DMA_SxCR_EN)) {
		_wan_write();
	}
	return 0;
}

// DMA发送完成中断
void DMA2_Stream7_IRQHandler(void)
{
	DMA_ClearFlag(DMA2_Stream7, DMA_FLAG_TCIF7);
	DMA_Cmd(DMA2_Stream7, DISABLE);
	
	_wan_write();
	return;
}

// DMA接收完成中断
void DMA2_Stream2_IRQHandler(void)
{
	DMA_ClearFlag(DMA2_Stream2, DMA_FLAG_TCIF2);
	DMA_Cmd(DMA2_Stream2, DISABLE);
	
	// push data to fifo
	wan_recv(1);

	DMA_SetCurrDataCounter(DMA2_Stream2, sizeof(dma_rxbuf));
	DMA_Cmd(DMA2_Stream2, ENABLE);	
	return;
}

// 空闲接收中断
void USART1_IRQHandler(void)
{
	u8 ReadData;
	u32 sr = USART1->SR;
	
	// 空闲中断
	if (sr & USART_FLAG_IDLE) {
		//u8 ReadData;
		// recv new data from dma
		//ReadData = USART2->DR;
		DMA_ITConfig(DMA2_Stream2, DMA_IT_TC, DISABLE);
		DMA_Cmd(DMA2_Stream2, DISABLE);
		DMA_ClearFlag(DMA2_Stream2, DMA_FLAG_TCIF2);
		wan_recv(0);
	}
	ReadData = USART1->DR;
	// over err
	if (sr & USART_FLAG_ORE) {
		ReadData = USART1->DR;
	}
	DMA_SetCurrDataCounter(DMA2_Stream2, sizeof(dma_rxbuf));
	DMA_ITConfig(DMA2_Stream2, DMA_IT_TC, ENABLE);
	DMA_Cmd(DMA2_Stream2, ENABLE);
	return;
}
// PC命令解析
void rs232_data_parse(u8 *data)
{
	u16 len;
	u8 param_buf[sizeof(rs232_param)];
	u8 param_req[sizeof(rs232_param) + FRAME_REGULAR_LEN];
	len = (u16)((data[LENGTH] << 8) + data[LENGTH + 1]);
	len -= FRAME_REGULAR_LEN;
	switch(data[CMD]) {
		case CMD_HANDSHAKE:
			break;
		case CMD_REQUEST:
			memcpy(param_buf, &rs232, sizeof(rs232));
			len = updata_send_pack(TYPE_RS232, CMD_REQUEST, param_buf, sizeof(rs232), param_req);
			VCP_DataTx(param_req, len);
			break;
		case CMD_SET:
			memcpy((u32 *)&rs232.baud, &data[DATA], sizeof(rs232.baud));
			rs232.stop_bit = data[DATA + sizeof(rs232.baud)];
			rs232.data_bit = data[DATA + sizeof(rs232.baud) + sizeof(rs232.stop_bit)];
			rs232.parity_bit = data[DATA + sizeof(rs232.baud) + sizeof(rs232.stop_bit) + sizeof(rs232.parity_bit)];
		
			DMA_ITConfig(DMA2_Stream7, DMA_IT_TC, DISABLE);
			DMA_Cmd(DMA2_Stream7, DISABLE);
			DMA_ClearFlag(DMA2_Stream7, DMA_FLAG_TCIF7);
			DMA_ITConfig(DMA2_Stream2, DMA_IT_TC, DISABLE);
			DMA_Cmd(DMA2_Stream2, DISABLE);
			DMA_ClearFlag(DMA2_Stream2, DMA_FLAG_TCIF2);
		
			USART_ITConfig(USART1, USART_IT_IDLE, DISABLE);
			USART_Cmd(USART1, DISABLE); 
			uart1_init(&rs232);			
			break;
		case CMD_SEND:
			memcpy(buf_parse, &data[DATA], len);
		//	printf("rs232_data_parse: recv data %d,%d,%d\n", buf_parse[0], buf_parse[len - 1], len);
			wan_write(buf_parse, len);
			break;
		case CMD_RECIVE:
			break;
		default :
			break;
	}
}
