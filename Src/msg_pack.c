#include "sys.h"
#include "msg_pack.h"
#include "usbd_cdc_vcp.h"
#include "sys_defines.h"
#include "kfifo.h"
#include "rs232dev.h"
#include "rs485dev.h"
#include "can.h"
#include "i2c.h"
#include "spi.h"
#include "adc.h"
#include "dac.h"
#include "pwm.h"

extern struct kfifo usb_rx_fifo;

u8 recv_buf[PC_USB_REC_LEN];//接收缓冲，从fifo读取数据
static u8 dev_buf[USB_PC_SEND_LEN];

u16 updata_send_pack(u8 type, u8 cmd, u8 *data, u16 data_num, u8 *buf)
{
	int i;
	u16 len = FRAME_REGULAR_LEN + data_num;
	buf[HEAD] = USB_HEAD;
	buf[LENGTH] = (u8)(len>>8);
	buf[LENGTH + 1] = (u8)len;
	buf[TYPE] = type;
	buf[CMD] = cmd;
	for (i = 0; i < data_num; i++) {
		buf[DATA + i] = data[i];
	}
	buf[len - 1] = USB_TAIL;
	return len;
}
void device_data_parse(u8 *data)
{
	u16 len;
	switch(data[CMD]) {
		case CMD_HANDSHAKE:
			//握手信息直接回传
			len = (u16)((data[1] << 8) + data[2]);
			memcpy(dev_buf, data, len);
			VCP_DataTx(dev_buf, len);
			break;
		case CMD_REQUEST:
			//返回版本号
			len = updata_send_pack(data[TYPE], data[CMD], verson, 2, dev_buf);
			VCP_DataTx(dev_buf, len);
			break;
		case CMD_SET:
			break;
		case CMD_SEND:
			break;
		case CMD_RECIVE:
			break;
		default :
			break;
	}
}
void type_parse(u8 *data)
{
	switch(data[TYPE]) {
		case TYPE_DEV:
			device_data_parse(data);
			break;
		case TYPE_CAN:
			can_data_parse(data);
			break;
		case TYPE_SPI:
			spi_data_parse(data);
			break;
		case TYPE_IIC:
			i2c_data_parse(data);
			break;
		case TYPE_ADC:
			adc_data_parse(data);
			break;
		case TYPE_PWM:
			pwm_data_parse(data);
			break;
		case TYPE_DAC:
			dac_data_parse(data);
			break;
		case TYPE_RS232:
			rs232_data_parse(data);
			break;
		case TYPE_RS485:
			rs485_data_parse(data);
			break;
		case TYPE_TTL:
			break;
		default :
			break;
	}
}
char get_recv_data(u8 *buf)
{
	u16 fifo_len, data_len;
	if ((fifo_len = kfifo_len(&usb_rx_fifo)) >= 3) {//fifo内字节数大于包头+帧长
		kfifo_prefetch(&usb_rx_fifo, buf, 3);//预取包头和帧长
		data_len = (u16)((buf[LENGTH] << 8) + buf[LENGTH + 1]);
		//校验包头，且fifo内数据长度不小于帧长
		if ((buf[HEAD] == USB_HEAD) && (fifo_len >= data_len)) {
			kfifo_get(&usb_rx_fifo, buf, data_len);//取出fifo数据
			if (buf[data_len - 1] == USB_TAIL) {
				return 1;
			}
		}
	}
	return 0;
}
void master_data_parse(void)
{
	if (get_recv_data(recv_buf) > 0) {
		type_parse(recv_buf);
	}
}
