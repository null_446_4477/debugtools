#include "stm32f4xx.h"
#include "stm32f4xx_conf.h"

#include "sys_defines.h"
#include "usbd_cdc_vcp.h"

#include "spi.h"
#include "delay.h"

#define udelay(x) delay_us(x)
#define mdelay(x) delay_ms(x)

static int data_size = SPI_DataSize_8b;
static int cpol = SPI_CPOL_Low;
static int cpha = SPI_CPHA_1Edge;
static int cs_pol = 0;			// 片选极性
static int clk_div = SPI_BaudRatePrescaler_64;	// SPI时钟频率
static int first_bit = SPI_FirstBit_MSB;
static int mode = SPI_Mode_Master;

static const int _div[] = {SPI_BaudRatePrescaler_2, SPI_BaudRatePrescaler_4, SPI_BaudRatePrescaler_8,
	SPI_BaudRatePrescaler_16, SPI_BaudRatePrescaler_32, SPI_BaudRatePrescaler_64,
	SPI_BaudRatePrescaler_128, SPI_BaudRatePrescaler_256};

#define SPICS_GPIO GPIOC
#define SPICS_PIN GPIO_Pin_4
// spi 模块

static void spi_cs_enable()
{
	if (cs_pol) {
		GPIO_SetBits(SPICS_GPIO, SPICS_PIN);
	} else {
		GPIO_ResetBits(SPICS_GPIO, SPICS_PIN);
	}
}

static void spi_cs_disable()
{
	if (cs_pol) {
		GPIO_ResetBits(SPICS_GPIO, SPICS_PIN);
	} else {
		GPIO_SetBits(SPICS_GPIO, SPICS_PIN);
	}
}

void SpiInit(void)
{
	SPI_InitTypeDef  SPI_InitStructure;
	GPIO_InitTypeDef gpio = {0};
	
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
	
	gpio.GPIO_Pin = SPICS_PIN;
	gpio.GPIO_Speed = GPIO_Speed_100MHz;
	gpio.GPIO_Mode = GPIO_Mode_OUT;
	gpio.GPIO_OType = GPIO_OType_PP;
	gpio.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(SPICS_GPIO, &gpio);

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI1, ENABLE);

	SPI_I2S_DeInit(SPI1);
	/* SPI1 Config -------------------------------------------------------------*/ 								  
	SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
	SPI_InitStructure.SPI_Mode = mode;
	SPI_InitStructure.SPI_DataSize = data_size;
	SPI_InitStructure.SPI_CPOL = cpol;
	SPI_InitStructure.SPI_CPHA = cpha;
	SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;		//片选为软件SPI 片选
	SPI_InitStructure.SPI_BaudRatePrescaler = clk_div;
	SPI_InitStructure.SPI_FirstBit = first_bit;
	SPI_InitStructure.SPI_CRCPolynomial = 7;
	SPI_Init(SPI1, &SPI_InitStructure);

	SPI_Cmd(SPI1, ENABLE); 
}

u16 SpiTransmit(SPI_TypeDef *dev, u16 dat)
{
	/* Loop while DR register in not emplty */
	while (SPI_I2S_GetFlagStatus(dev, SPI_I2S_FLAG_TXE) == RESET);

	/* Send byte through the SPI2 peripheral */
	SPI_I2S_SendData(dev, dat);
    
	/* Wait to receive a byte */
	while (SPI_I2S_GetFlagStatus(dev, SPI_I2S_FLAG_RXNE) == RESET);
	
	/* Return the byte read from the SPI bus */
	return  SPI_I2S_ReceiveData(dev);
}

u16 SpiTransmitContinous(SPI_TypeDef *dev, void *txbuf, void *rxbuf, u16 len)
{
	u16 i;

	for (i = 0; i < len; i++) {
		/* Loop while DR register in not emplty */
		while (SPI_I2S_GetFlagStatus(dev, SPI_I2S_FLAG_TXE) == RESET);
		/* Send byte through the SPI2 peripheral */
		if (data_size == SPI_DataSize_8b) {
			SPI_I2S_SendData(dev, ((u8 *)txbuf)[i]);
		} else {
			SPI_I2S_SendData(dev, ((u16 *)txbuf)[i]);
		}

		/* Wait to receive a byte */
		while (SPI_I2S_GetFlagStatus(dev, SPI_I2S_FLAG_RXNE) == RESET);

		/* Return the byte read from the SPI bus */
		if (data_size == SPI_DataSize_8b) {
			((u8 *)rxbuf)[i] = SPI_I2S_ReceiveData(dev);
		} else {
			((u16 *)rxbuf)[i] = SPI_I2S_ReceiveData(dev);
		}
	}
	return i;
}

// spi命令解析
void spi_data_parse(u8 *data)
{
	u16 len;
	len = (u16)((data[LENGTH] << 8) + data[LENGTH + 1]);
	switch (data[CMD]) {
		case 4:
			// spi transfer
			spi_cs_enable();
			if (data_size == SPI_DataSize_8b) {
				SpiTransmitContinous(SPI1, data + DATA, data + DATA, len - 6);
			} else {
				SpiTransmitContinous(SPI1, data + DATA, data + DATA, (len - 6) / 2);
			}
			spi_cs_disable();
			data[CMD] = 0x5;
			VCP_DataTx(data, len);
			break;
		case 3:
			// 配置spi
			if (data[DATA] == 0) {
				mode = SPI_Mode_Master;
			} else {
				mode = SPI_Mode_Slave;
			}
			if (data[DATA + 1] == 0) {
				data_size = SPI_DataSize_8b;
			} else {
				data_size = SPI_DataSize_16b;
			}
			if (data[DATA + 2] == 0) {
				cpol = SPI_CPOL_Low;
			} else {
				cpol = SPI_CPOL_High;
			}
			if (data[DATA + 3] == 0) {
				cpha = SPI_CPHA_1Edge;
			} else {
				cpha = SPI_CPHA_2Edge;
			}
			if (data[DATA + 4] == 0) {
				cs_pol = 0;
			} else {
				cs_pol = 1;
			}
			data[DATA + 5] &= 0x7;
			clk_div = _div[data[DATA + 5]];
			if (data[DATA + 6] == 0) {
				first_bit = SPI_FirstBit_LSB;
			} else {
				first_bit = SPI_FirstBit_MSB;
			}
			SpiInit();
			break;
		case 2:
			// 读参数
			{
				u8 buf[13] = {0xfa, 0, 13, 2, 3};
				buf[12] = 0xfd;
				buf[DATA] = (mode == SPI_Mode_Master) ? 0 : 1;
				buf[DATA + 1] = (data_size == SPI_DataSize_8b) ? 0 : 1;
				buf[DATA + 2] = (cpol == SPI_CPOL_Low) ? 0 : 1;
				buf[DATA + 3] = (cpha == SPI_CPHA_1Edge) ? 0 : 1;
				buf[DATA + 4] = (cs_pol == 0) ? 0 : 1;
				buf[DATA + 5] = clk_div >> 3;
				buf[DATA + 5] &= 0x7;
				buf[DATA + 6] = (first_bit == SPI_FirstBit_LSB) ? 0 : 1;
				VCP_DataTx(buf, sizeof(buf));
			}
			break;
		default :
			break;
	}
	return;
}

