#include "stm32f4xx.h"
#include "stm32f4xx_conf.h"

#include "sys_defines.h"
#include "usbd_cdc_vcp.h"

#include "pwm.h"
#include "delay.h"

#define udelay(x) delay_us(x)
#define mdelay(x) delay_ms(x)

static int pwm_freq = 1000;	// 频率
static int pwm_duty = 50;	// 占空比
static int pwm_en = 0;
static int pclk2 = 84000000 * 2;

// 目标间隔sample_interval
static int cal_tim_regs(TIM_TimeBaseInitTypeDef *tb)
{
	// 目标时钟
	int clk = pclk2;
	double period = 1.0 / clk;
	double interval = 1.0 / pwm_freq;
	double tmax = interval / 65536;		// clk最大周期
	double psc = clk * tmax;		// prescaler
	int prescaler = psc;		// 取整
	double clk2 = (double)clk / (prescaler + 1);
	double n = interval * clk2;
	tb->TIM_ClockDivision = 0;
	tb->TIM_Prescaler = prescaler;
	tb->TIM_Period = n + 0.5;
	return 0;
}


// pwm 模块
void pwm_init(void)
{
	GPIO_InitTypeDef gpio = {0};
	
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);

	// pa8 tim1_ch1
	gpio.GPIO_Pin = GPIO_Pin_8;
	gpio.GPIO_Speed = GPIO_Speed_100MHz;
	gpio.GPIO_Mode = GPIO_Mode_AF;
	gpio.GPIO_OType = GPIO_OType_PP;
	gpio.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOA, &gpio);
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource8, GPIO_AF_TIM1);

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1, ENABLE);
}

int pwm_out(int en)
{
	TIM_TimeBaseInitTypeDef tb;
	TIM_OCInitTypeDef  TIM_OCInitStructure;

	TIM_DeInit(TIM1);
	if (en == 0) {
		return 0;
	}
	// pclk2 -> 84M
	memset(&tb, 0, sizeof(tb));
	tb.TIM_CounterMode = TIM_CounterMode_Up;
	tb.TIM_RepetitionCounter = 0;
	cal_tim_regs(&tb);
	TIM_TimeBaseInit(TIM1, &tb);

	TIM_ClearFlag(TIM1, TIM_FLAG_Update);

	/* PWM1 Mode configuration: Channel1 */
	memset(&TIM_OCInitStructure, 0, sizeof(TIM_OCInitStructure));
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
//	TIM_OCInitStructure.TIM_OutputNState = TIM_OutputNState_Enable;
	TIM_OCInitStructure.TIM_Pulse = tb.TIM_Period * pwm_duty / 100;
	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;

	TIM_OC1Init(TIM1, &TIM_OCInitStructure);
	TIM_OC1PreloadConfig(TIM1, TIM_OCPreload_Enable);
	TIM_ARRPreloadConfig(TIM1, ENABLE);
	TIM_Cmd(TIM1, ENABLE);
	TIM_CtrlPWMOutputs(TIM1, ENABLE);
	return 0;
}

// pwm命令解析
void pwm_data_parse(u8 *data)
{
//	u16 len;
//	len = (u16)((data[LENGTH] << 8) + data[LENGTH + 1]);
	switch (data[CMD]) {
		case 3:
			// 配置pwm
			pwm_en = data[DATA];
			memcpy(&pwm_freq, data + DATA + 1, 4);
			pwm_duty = data[DATA + 5];
			pwm_out(pwm_en);
			break;
		case 2:
			// 读参数
			{
				u8 buf[12] = {0xfa, 0, 13, 2, 3};
				buf[11] = 0xfd;
				buf[DATA] = pwm_en;
				memcpy(data + DATA + 1, &pwm_freq, 4);
				buf[DATA + 5] = pwm_duty;
				VCP_DataTx(buf, sizeof(buf));
			}
			break;
		default :
			break;
	}
	return;
}

