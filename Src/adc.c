#include "stm32f4xx.h"
#include "stm32f4xx_conf.h"
#include "adc.h"
#include "kfifo.h"
#include "msg_pack.h"
#include "sys_defines.h"
#include "usbd_cdc_vcp.h"

#define ADC_BUF_SIZE 509
static int sample_interval = 100000;	// 采样间隔ns, min 714.3ns
static u16 store_depth = 1000;		// 存储深度
static const int pclk2 = 84000000 * 2;		// pclk2 84M
static u16 adc_buf[ADC_BUF_SIZE];			// 2k adc buffer
static int start = 0;

static int cnt = 0;		// 传输计次
static int cnti = 0;

// 目标间隔sample_interval
static int cal_tim_regs(TIM_TimeBaseInitTypeDef *tb)
{
	// 目标时钟
	int clk = pclk2;
	double period = 1.0 / clk;
	double interval = sample_interval / 1000000000.0;
	double tmax = interval / 65536;		// clk最大周期
	double psc = clk * tmax;		// prescaler
	int prescaler = psc;		// 取整
	double clk2 = (double)clk / (prescaler + 1);
	double n = interval * clk2;
	tb->TIM_ClockDivision = 0;
	tb->TIM_Prescaler = prescaler;
	tb->TIM_Period = n + 0.5;

	// 计算cnt
	interval *= ADC_BUF_SIZE;
	cnt = 0.02 / interval;
	return 0;
}

// use tim8 trgo
static void adc_tim_config()
{
	TIM_TimeBaseInitTypeDef tb;
	// tim8 clock enable
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM8, ENABLE);

	TIM_DeInit(TIM8);
	// pclk2 -> 84M
	memset(&tb, 0, sizeof(tb));
	tb.TIM_CounterMode = TIM_CounterMode_Up;
	tb.TIM_RepetitionCounter = 0;
	cal_tim_regs(&tb);
	TIM_TimeBaseInit(TIM8, &tb);
	TIM_SelectOutputTrigger(TIM8, TIM_TRGOSource_Update);
	TIM_ClearFlag(TIM8, TIM_FLAG_Update);
	TIM_Cmd(TIM8, ENABLE);

	return;
}

static void adc_config(void)
{
	ADC_InitTypeDef       ADC_InitStructure;
	ADC_CommonInitTypeDef ADC_CommonInitStructure;

	ADC_DeInit();
	/* ADC Common Init **********************************************************/
	ADC_CommonInitStructure.ADC_Mode = ADC_Mode_Independent;
	ADC_CommonInitStructure.ADC_Prescaler = ADC_Prescaler_Div4;		// pclk2 / 4 = 21M
	ADC_CommonInitStructure.ADC_DMAAccessMode = ADC_DMAAccessMode_Disabled;
	ADC_CommonInitStructure.ADC_TwoSamplingDelay = ADC_TwoSamplingDelay_5Cycles;
	ADC_CommonInit(&ADC_CommonInitStructure);

	ADC_StructInit(&ADC_InitStructure);
	/* ADC1 Init ****************************************************************/
	ADC_InitStructure.ADC_Resolution = ADC_Resolution_12b;
	ADC_InitStructure.ADC_ScanConvMode = DISABLE;
	ADC_InitStructure.ADC_ContinuousConvMode = DISABLE;	// TIM TRIG
	ADC_InitStructure.ADC_ExternalTrigConvEdge = ADC_ExternalTrigConvEdge_Rising;
	ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_T8_TRGO;
	ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
	ADC_InitStructure.ADC_NbrOfConversion = 1;
	ADC_Init(ADC1, &ADC_InitStructure);
}

static void adc_dma_config()
{
	DMA_InitTypeDef       DMA_InitStructure;
	/* DMA2 Stream0 channel0 configuration **************************************/
	DMA_InitStructure.DMA_Channel = DMA_Channel_0;
	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)(&ADC1->DR);
	DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t)adc_buf;
	DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralToMemory;
	DMA_InitStructure.DMA_BufferSize = ADC_BUF_SIZE;
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
	DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
	DMA_InitStructure.DMA_Priority = DMA_Priority_High;
	DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable;
	DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_HalfFull;
	DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
	DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
	DMA_Init(DMA2_Stream0, &DMA_InitStructure);
	
	// 设置DMA中断模式
	DMA_ITConfig(DMA2_Stream0, DMA_IT_TC, ENABLE);
	
	DMA_Cmd(DMA2_Stream0, ENABLE);
	return;
}

void adc_init()
{
	GPIO_InitTypeDef      GPIO_InitStructure;
	NVIC_InitTypeDef nvic;
	// enable clk
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);
	
	/* Configure ADC1 Channel4 pin as analog input ******************************/
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL ;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	// config dma nvic
	nvic.NVIC_IRQChannel = DMA2_Stream0_IRQn;
	nvic.NVIC_IRQChannelPreemptionPriority = 0;
	nvic.NVIC_IRQChannelSubPriority = 0;
	nvic.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&nvic);
}

/**
  * @brief  ADC12 channe4 with DMA configuration
  * @note   This function Configure the ADC peripheral
            1) Enable peripheral clocks
            2) DMA2_Stream0 channel0 configuration
            3) Configure ADC Channel4 pin as analog input
            4) Configure ADC12 Channel4
  * @param  None
  * @retval None
  */
void adc_start(void)
{
	adc_config();
	adc_dma_config();
	/* ADC1 regular channel4 configuration **************************************/
	ADC_RegularChannelConfig(ADC1, ADC_Channel_4, 1, ADC_SampleTime_3Cycles);

	/* Enable DMA request after last transfer (Single-ADC mode) */
	ADC_DMARequestAfterLastTransferCmd(ADC1, ENABLE);

	/* Enable ADC1 DMA */
	ADC_DMACmd(ADC1, ENABLE);

	/* Enable ADC1 */
	ADC_Cmd(ADC1, ENABLE);
	
	adc_tim_config();
	ADC_SoftwareStartConv(ADC1);
}

void adc_stop()
{
	TIM_Cmd(TIM8, DISABLE);
	ADC_Cmd(ADC1, DISABLE);
	// 设置DMA中断模式
	DMA_ITConfig(DMA2_Stream0, DMA_IT_TC, DISABLE);
	DMA_Cmd(DMA2_Stream0, DISABLE);
}

static void adc_send()
{
	u8 buf[5] = {0xFA, 0x0, 0x0, 0x4, 0x5};
	int n = sizeof(adc_buf) + 6;
	if (n > VCP_Buffer_avail()) {
		return;
	}
	buf[1] = n >> 8;
	buf[2] = n & 0xFF;
	VCP_DataTx(buf, 5);
	VCP_DataTx((u8 *)adc_buf, sizeof(adc_buf));
	buf[0] = 0xFD;
	VCP_DataTx(buf, 1);
	return;
}

// DMA接收完成中断
void DMA2_Stream0_IRQHandler(void)
{
	DMA_ClearFlag(DMA2_Stream0, DMA_FLAG_TCIF0);
	DMA_Cmd(DMA2_Stream0, DISABLE);
	
	// send adc data
	if (cnti == cnt) {
		adc_send();
		cnti = 0;
	} else {
		cnti++;
	}
	if (ADC_GetFlagStatus(ADC1, ADC_FLAG_OVR) == SET) {
		ADC_ClearFlag(ADC1, ADC_FLAG_OVR);
	}
	DMA_SetCurrDataCounter(DMA2_Stream0, ADC_BUF_SIZE);
	DMA_Cmd(DMA2_Stream0, ENABLE);
}

// PC命令解析
void adc_data_parse(u8 *data)
{
	u8 buf[13] = {0xFA, 0x0, 0xD, 0x4,
		0x3, start, 0, 0, 0, 0, 0, 0, 0xfd};
	switch(data[CMD]) {
		case 3:
			// 配置参数
			start = data[5];
			memcpy(&sample_interval, data + 6, 4);
			memcpy(&store_depth, data + 10, 2);
			if (start) {
				adc_stop();
				adc_start();
			} else {
				adc_stop();
			}
			break;
		case 2:
			// 查询参数
			memcpy(buf + 6, &sample_interval, 4);
			memcpy(buf + 10, &store_depth, 2);
			VCP_DataTx(buf, sizeof(buf));
			break;
		default :
			break;
	}
	return;
}

