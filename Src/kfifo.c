#include "kfifo.h"

void kfifo_lock(void)
{
	INTX_DISABLE();
	//__disable_irq();
}

void kfifo_unlock(void)
{
	INTX_ENABLE();
	//__enable_irq();
}

int kfifo_init(struct kfifo *fifo, unsigned char *buffer, unsigned int size)
{
	/* size must be a power of 2 */
	if (size & (size - 1)) {
		return -1;
	}

	fifo->buffer = buffer;
	fifo->size = size;
	fifo->in = fifo->out = 0;

	fifo->lock = kfifo_lock;
	fifo->unlock = kfifo_unlock;
	return 0;
}

unsigned int kfifo_put(struct kfifo *fifo,
			 unsigned char *buffer, unsigned int len)
{
	unsigned int l;

	fifo->lock();
	
	len = min(len, fifo->size - fifo->in + fifo->out);

	/* first put the data starting from fifo->in to buffer end */
	l = min(len, fifo->size - (fifo->in & (fifo->size - 1)));
	memcpy(fifo->buffer + (fifo->in & (fifo->size - 1)), buffer, l);

	/* then put the rest (if any) at the beginning of the buffer */
	memcpy(fifo->buffer, buffer + l, len - l);

	fifo->in += len;

	fifo->unlock();
	return len;
}

unsigned int kfifo_get(struct kfifo *fifo,
			 unsigned char *buffer, unsigned int len)
{
	unsigned int l;

	fifo->lock();
	len = min(len, fifo->in - fifo->out);

	/* first get the data from fifo->out until the end of the buffer */
	l = min(len, fifo->size - (fifo->out & (fifo->size - 1)));
	memcpy(buffer, fifo->buffer + (fifo->out & (fifo->size - 1)), l);

	/* then get the rest (if any) from the beginning of the buffer */
	memcpy(buffer + l, fifo->buffer, len - l);

	fifo->out += len;

	/*
	 * optimization: if the FIFO is empty, set the indices to 0
	 * so we don't wrap the next time
	 */
	if (fifo->in == fifo->out)
		fifo->in = fifo->out = 0;
	
	fifo->unlock();
	
	return len;
}

/*
 * 取出字节，但不改变指针
 */
unsigned int kfifo_prefetch(struct kfifo *fifo, unsigned char *buffer, unsigned int len)
{
	unsigned int l;

	fifo->lock();
	len = min(len, fifo->in - fifo->out);

	/* first get the data from fifo->out until the end of the buffer */
	l = min(len, fifo->size - (fifo->out & (fifo->size - 1)));
	memcpy(buffer, fifo->buffer + (fifo->out & (fifo->size - 1)), l);

	/* then get the rest (if any) from the beginning of the buffer */
	memcpy(buffer + l, fifo->buffer, len - l);

	fifo->unlock();
	return len;
}

/* 移动KFIFO指针 */
int kfifo_cut(struct kfifo *fifo, unsigned int len)
{
	fifo->lock();
	if (len > (fifo->in - fifo->out)) {
		fifo->unlock();
		return -1;
	}
	
	fifo->out += len;
	/*
	 * optimization: if the FIFO is empty, set the indices to 0
	 * so we don't wrap the next time
	 */
	if (fifo->in == fifo->out)
		fifo->in = fifo->out = 0;
	fifo->unlock();
	return 0;
}

unsigned int kfifo_len(struct kfifo *fifo)
{
	unsigned int len;
	fifo->lock();
	len = fifo->in - fifo->out;
	fifo->unlock();
	return len;
}

void kfifo_reset(struct kfifo *fifo)
{
	fifo->lock();
	fifo->in = fifo->out = 0;
	fifo->unlock();
}

/**
 * kfifo_avail - returns the number of bytes available in the FIFO
 * @fifo: the fifo to be used.
 */
unsigned int kfifo_avail(struct kfifo *fifo)
{
	return fifo->size - kfifo_len(fifo);
}

/*
 * 从最后存入开始删除n字节
 */
unsigned int kfifo_back(struct kfifo *fifo, int len)
{
	int l;
	fifo->lock();
	l = min(kfifo_len(fifo), len);
	fifo->in -= l;
	fifo->unlock();
	return 0;
}

