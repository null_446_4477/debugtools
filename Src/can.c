#include "stm32f4xx.h"
#include "can.h"
#include "sys.h"
#include "kfifo.h"
#include "usbd_cdc_vcp.h"
#include "msg_pack.h"
#include "sys_defines.h"

struct kfifo can1_fifo;
static u8 tx_buf_channel_1[MAX_CAN_BUF_SIZE];
struct kfifo can2_fifo;
static u8 tx_buf_channel_2[MAX_CAN_BUF_SIZE];

static u8 rx_buf[MAX_CAN_BUF_SIZE];

static can_param can_channel_1 = {4, 0, 0x00000000, 0x00000000};
static can_param can_channel_2 = {4, 0, 0x00000000, 0x00000000};

static u8 buf_pack[MAX_CAN_BUF_SIZE + FRAME_REGULAR_LEN];//接收到的数据进行打包的buffer
static u8 buf_parse[USB_PC_SEND_LEN];
static int baud_config[9] = {//波特率配置，BRP选项，SJW\TS2\TS1为固定1，3，3
//	20k  50k  100k125k200k250k400k500k1M
	300, 120, 60, 48, 30, 24, 15, 12, 6
};
/*=====================================================>
Function		:	Init GPIO and NVIC for CAN test.
Description	: 
Return	 	:
<=====================================================*/
void CAN_CANGPIO_NVIC_Config(void)
{
	NVIC_InitTypeDef  NVIC_InitStructure;
	GPIO_InitTypeDef  GPIO_InitStructure;

	/* Configure NVIC CAN1 RX interrupt */
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_0);
	NVIC_InitStructure.NVIC_IRQChannel = CAN1_RX0_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	/* Configure NVIC CAN1 TX interrupt */
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_0);
	NVIC_InitStructure.NVIC_IRQChannel = CAN1_TX_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	
	/* Configure NVIC CAN2 RX interrupt */
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_0);
	NVIC_InitStructure.NVIC_IRQChannel = CAN2_RX1_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	/* Configure NVIC CAN2 TX interrupt */
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_0);
	NVIC_InitStructure.NVIC_IRQChannel = CAN2_TX_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	/* Configure CAN1 pin: RX */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(GPIOB, &GPIO_InitStructure);// GPIOB

	/* Configure CAN1 pin: TX */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOB, &GPIO_InitStructure);// GPIOB
	
	/* Configure CAN2 pin: RX */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(GPIOB, &GPIO_InitStructure);// GPIOB

	/* Configure CAN2 pin: TX */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOB, &GPIO_InitStructure);// GPIOB

	GPIO_PinAFConfig(GPIOB, GPIO_PinSource8, GPIO_AF_CAN1);
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource9, GPIO_AF_CAN1);
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource12, GPIO_AF_CAN2);
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource13, GPIO_AF_CAN2);
}

/*=====================================================>
Function		:	 Configures the CAN.
Description	: 
Return	 	:
<=====================================================*/
void CAN_Config(CAN_TypeDef * can, can_param *param)
{
	CAN_InitTypeDef		  CAN_InitStructure;
	CAN_FilterInitTypeDef  CAN_FilterInitStructure;
	/* CAN register init */
	CAN_DeInit(can);
	CAN_StructInit(&CAN_InitStructure);

	/* CAN1 cell init */
	CAN_InitStructure.CAN_TTCM = DISABLE;
	CAN_InitStructure.CAN_ABOM = DISABLE;
	CAN_InitStructure.CAN_AWUM = DISABLE;
	CAN_InitStructure.CAN_NART = DISABLE;
	CAN_InitStructure.CAN_RFLM = DISABLE;
	CAN_InitStructure.CAN_TXFP = DISABLE;
	CAN_InitStructure.CAN_Mode = CAN_Mode_Normal;
	CAN_InitStructure.CAN_SJW = CAN_SJW_1tq;
	CAN_InitStructure.CAN_BS1 = CAN_BS1_3tq;
	CAN_InitStructure.CAN_BS2 = CAN_BS2_3tq;
	CAN_InitStructure.CAN_Prescaler = baud_config[param->baud - 1];
	CAN_Init(can, &CAN_InitStructure);
// 波特率计算公式
//CPU 是168M 速度,PCLK 是42M
// 波特率为42M/（CAN_Prescaler*（CAN_SJW+CAN_BS1+CAN_BS2))
//     42M/6*(1+3+3)=1M波特率 
//     42M/12*(1+3+3)=500k波特率 
//     42M/15*(1+3+3)=400k波特率 
//     42M/24*(1+3+3)=250k波特率 
//     42M/30*(1+3+3)=200k波特率 
//     42M/48*(1+3+3)=125k波特率 
//     42M/60*(1+3+3)=100k波特率 
//     42M/120*(1+3+3)=50k波特率 
//     42M/300*(1+3+3)=20k波特率 
	/* CAN filter init */
	if (can == CAN1) {
		CAN_FilterInitStructure.CAN_FilterNumber=0;
		CAN_FilterInitStructure.CAN_FilterFIFOAssignment=0;
	} else {
		CAN_FilterInitStructure.CAN_FilterNumber=14;
		CAN_FilterInitStructure.CAN_FilterFIFOAssignment=1;
	}
//	CAN_FilterInitStructure.CAN_FilterNumber=param->filt_num;
	CAN_FilterInitStructure.CAN_FilterMode=CAN_FilterMode_IdMask;
	CAN_FilterInitStructure.CAN_FilterScale=CAN_FilterScale_32bit;
	CAN_FilterInitStructure.CAN_FilterIdHigh=(u16)(param->filter>>16);
	CAN_FilterInitStructure.CAN_FilterIdLow=(u16)param->filter;
	CAN_FilterInitStructure.CAN_FilterMaskIdHigh=(u16)(param->mask>>16);
	CAN_FilterInitStructure.CAN_FilterMaskIdLow=(u16)param->mask;
//	CAN_FilterInitStructure.CAN_FilterFIFOAssignment=0;
	CAN_FilterInitStructure.CAN_FilterActivation=ENABLE;
	CAN_FilterInit(&CAN_FilterInitStructure);
}

 /*=====================================================>
Function		:	Init GPIO and NVIC for CAN test.
Description	: 
Return	 	:
<=====================================================*/
void CANBUS_Config(void)
{
	/* GPIO clock enable */
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
	/* CAN1 Periph clock enable */
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_CAN1, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_CAN2, ENABLE);
	//配置IO,中断
	CAN_CANGPIO_NVIC_Config();
	CAN_Config(CAN1, &can_channel_1);
	CAN_Config(CAN2, &can_channel_2);
	
	kfifo_init(&can1_fifo, tx_buf_channel_1, MAX_CAN_BUF_SIZE);
	kfifo_init(&can2_fifo, tx_buf_channel_2, MAX_CAN_BUF_SIZE);
	
	/* IT Configuration for CAN1 RX */  
	CAN_ITConfig(CAN1, CAN_IT_FMP0, ENABLE);
	/* IT Configuration for CAN1 TX */
	CAN_ITConfig(CAN1, CAN_IT_TME, ENABLE);
	/* IT Configuration for CAN1 RX */  
	CAN_ITConfig(CAN2, CAN_IT_FMP1, ENABLE);
	/* IT Configuration for CAN1 TX */
	CAN_ITConfig(CAN2, CAN_IT_TME, ENABLE);
}
/* 关闭/打开中断 */
void can_interrupt(CAN_TypeDef * can, FunctionalState stat)
{
	if (can == CAN1) {
		/* IT Configuration for CAN1 RX */	
		CAN_ITConfig(can, CAN_IT_FMP0, stat);
	} else {
		/* IT Configuration for CAN1 RX */	
		CAN_ITConfig(can, CAN_IT_FMP1, stat);
	}
	/* IT Configuration for CAN1 TX */ 
	CAN_ITConfig(can, CAN_IT_TME, stat);
}

// can_frame to turn CanTxMsg
void to_be_CanTxMsg(CanTxMsg *tx_msg, struct can_frame *msg)
{
	tx_msg->IDE = (msg->ide) ? CAN_Id_Extended : CAN_Id_Standard;
	if (tx_msg->IDE == CAN_Id_Standard) {
		tx_msg->StdId = msg->id;
		tx_msg->ExtId = 0;
	} else {
		tx_msg->StdId = 0;
		tx_msg->ExtId = msg->id;
	}
	tx_msg->RTR = (msg->rtr) ? CAN_RTR_REMOTE : CAN_RTR_DATA;
	tx_msg->DLC = msg->dlc;
	memcpy(tx_msg->Data, msg->data, 8);
}

// CanRxMsg to turn can_frame
void to_be_CanFrame(CanRxMsg *rx_msg, struct can_frame *msg)
{
	msg->ide = (rx_msg->IDE == CAN_Id_Extended) ? 1 : 0;
	if (msg->ide) {
		msg->id = rx_msg->ExtId;
	} else {
		msg->id = rx_msg->StdId;
	}
	msg->rtr = (rx_msg->RTR == CAN_RTR_REMOTE) ? 1 : 0;
	msg->dlc = rx_msg->DLC;
	memcpy(msg->data, rx_msg->Data, 8);
}

/* write messages, ???? */
static inline uint8_t send_can_msg(CAN_TypeDef * can, struct can_frame *msg)
{
	uint8_t status;
	CanTxMsg TxMessage;
	to_be_CanTxMsg(&TxMessage, msg);
	while (1) {
		status = CAN_Transmit(can, &TxMessage);
		if (status != CAN_NO_MB) {
			break;
		}
	}
	return status;
}

int can_write_msg(CAN_TypeDef * can)
{
	struct can_frame can_msg;
	struct kfifo *fifo;
	can_interrupt(can, DISABLE);
	if (can == CAN1) {
		fifo = &can1_fifo;
	} else if (can == CAN2) {
		fifo = &can2_fifo;
	}
	if (kfifo_len(fifo) >= sizeof(can_msg)) {
		kfifo_get(fifo, (u8 *)&can_msg, sizeof(can_msg));
	} else {
		can_interrupt(can, ENABLE);
		return -1;
	}
	send_can_msg(can, &can_msg);
	
	can_interrupt(can, ENABLE);
	return 0;
}
void canopen_recv(CAN_TypeDef * can)
{
	struct can_frame msg;
	CanRxMsg RxMessage;
	u16 len;

	if (can == CAN1) {
		CAN_Receive(can, CAN_FIFO0, &RxMessage);
	} else {
		CAN_Receive(can, CAN_FIFO1, &RxMessage);
	}
	to_be_CanFrame(&RxMessage, &msg);

	/* 处理数据 */
	//通道
	rx_buf[0] = (can == CAN1)? 1 : 2;
	//ID
	memcpy(&rx_buf[1], &msg.id, sizeof(msg.id));
	//帧类型和长度
	rx_buf[sizeof(msg.id) + 1] = (msg.ide<<7) + (msg.rtr<<6) + msg.dlc;
	//数据
	memcpy(&rx_buf[sizeof(msg.id) + 2], msg.data, sizeof(msg.data));
	len = updata_send_pack(TYPE_CAN, CMD_RECIVE, rx_buf, 14, buf_pack);
	VCP_DataTx(buf_pack, len);
}
void CAN1_RX0_IRQHandler(void)
{
	canopen_recv(CAN1);
}
void CAN1_TX_IRQHandler(void)
{
	u16 len; 
	if (CAN1->TSR & 0x10101) {
		CAN1->TSR |= CAN1->TSR & 0x10101;
	}
	len = kfifo_len(&can1_fifo);
	if (len) {
		if (can_write_msg(CAN1) < 0) {
		}
	}
}
void CAN2_RX1_IRQHandler(void)
{
	canopen_recv(CAN2);
}
void CAN2_TX_IRQHandler(void)
{
	u16 len; 
	if (CAN2->TSR & 0x10101) {
		CAN2->TSR |= CAN2->TSR & 0x10101;
	}
	len = kfifo_len(&can2_fifo);
	if (len) {
		if (can_write_msg(CAN2) < 0) {
		}
	}
}
// PC命令解析
void can_data_parse(u8 *data)
{
	u16 len;
	struct can_frame msg;
	can_param *can;
	CAN_TypeDef * can_channel;
	struct kfifo *fifo;
	u8 param_buf[sizeof(can_param)];
	u8 param_req[sizeof(can_param) + FRAME_REGULAR_LEN];
	len = (u16)((data[LENGTH] << 8) + data[LENGTH + 1]);
	len -= FRAME_REGULAR_LEN;
	switch(data[CMD]) {
		case CMD_HANDSHAKE:
			break;
		case CMD_REQUEST:
			param_buf[0] = data[DATA];
			if (param_buf[0] == 1) {
				memcpy(&param_buf[1], &can_channel_1, sizeof(can_param));
			} else {
				memcpy(&param_buf[1], &can_channel_2, sizeof(can_param));
			}
			len = updata_send_pack(TYPE_CAN, CMD_REQUEST, param_buf, sizeof(can_param), param_req);
			VCP_DataTx(param_req, len);
			break;
		case CMD_SET:
			if (data[DATA] == 1) {
				can = &can_channel_1;
				can_channel = CAN1;
			} else {
				can = &can_channel_2;
				can_channel = CAN2;
			}
			can->baud = data[DATA + 1];
			can->filt_num = data[DATA + 2];
			memcpy((u32 *)&can->filter, &data[DATA + 3], sizeof(can->filter));
			memcpy((u32 *)&can->mask, &data[DATA + 3 + sizeof(can->filter)], sizeof(can->mask));
			can_interrupt(can_channel, DISABLE);
			CAN_Config(can_channel, can);
			can_interrupt(can_channel, ENABLE);
			break;
		case CMD_SEND:
			memset(buf_parse, 0, sizeof(buf_parse));
			memset(&msg, 0, sizeof(msg));
			memcpy(&msg.id, &data[DATA + 1], sizeof(msg.id));
			msg.ide = (data[DATA + 1 + sizeof(msg.id)] & 0x80)?1:0;
			msg.rtr = (data[DATA + 1 + sizeof(msg.id)] & 0x40)?1:0;
			msg.dlc = data[DATA + 1 + sizeof(msg.id)] & 0x0F;
			memcpy(msg.data, &data[DATA + 2 + sizeof(msg.id)], msg.dlc);
			memcpy(buf_parse, &msg, sizeof(msg));
		//	printf("rs232_data_parse: recv data %d,%d,%d\n", buf_parse[0], buf_parse[len - 1], len);
			if (data[DATA] == 1) {
				can_channel = CAN1;
				fifo = &can1_fifo;
			} else {
				can_channel = CAN2;
				fifo = &can2_fifo;
			}
			if (kfifo_avail(fifo) >= sizeof(msg)) {
				kfifo_put(fifo, buf_parse, sizeof(msg));
				can_write_msg(can_channel);
			}
			break;
		case CMD_RECIVE:
			break;
		default :
			break;
	}
}
