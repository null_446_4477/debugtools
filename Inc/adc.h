#ifndef ADC_H
#define ADC_H

void adc_init(void);
void adc_start(void);
void adc_stop(void);
void adc_data_parse(u8 *data);


#endif

