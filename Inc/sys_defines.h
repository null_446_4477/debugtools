#ifndef _SYS_DEFINES_H
#define _SYS_DEFINES_H

#define USB_HEAD	0xfa//数据包头
#define USB_TAIL	0xFD//数据包尾

#define TYPE_DEV 0X00
#define TYPE_CAN 0X01
#define TYPE_SPI 0X02
#define TYPE_IIC 0X03
#define TYPE_ADC 0X04
#define TYPE_PWM 0X05
#define TYPE_DAC 0X06
#define TYPE_RS232 0X07
#define TYPE_RS485 0X08
#define TYPE_TTL 0X09

#define CMD_HANDSHAKE 0X01
#define CMD_REQUEST 0X02
#define CMD_SET 0X03
#define CMD_SEND 0X04
#define CMD_RECIVE 0X05

#define FRAME_REGULAR_LEN 6 //协议内包头 + 帧长 + 类型 + 命令 + 包尾  的长度

#define USART_REC_LEN  			2048  	//定义最大接收字节数 200
enum {
	HEAD = 0,
	LENGTH = 1,
	TYPE = 3,
	CMD = 4,
	DATA = 5,
};

#endif
