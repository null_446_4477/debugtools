#ifndef _DEVICE_H_
#define _DEVICE_H_

#include "usb_core.h"

void Error_Handler(void);
void SystemClock_Config(void);
void MX_ADC1_Init(void);
void MX_CAN1_Init(void);
void MX_CAN2_Init(void);
void MX_DAC_Init(void);
void MX_I2C1_Init(void);
void MX_RTC_Init(void);
void MX_SPI1_Init(void);
void MX_TIM1_Init(void);
void MX_UART4_Init(void);
void MX_UART5_Init(void);
void MX_USART1_UART_Init(void);
void MX_USART2_UART_Init(void);
void MX_USART6_UART_Init(void);
void MX_GPIO_Init(void);

#endif
