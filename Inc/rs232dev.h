#ifndef _RS232DEV_H
#define _RS232DEV_H
#include "sys.h"

#define EN_USART1_RX 			1		//使能（1）/禁止（0）串口1接收

typedef struct _rs232_param {
	u32 baud;
	u8 stop_bit;
	u8 data_bit;
	u8 parity_bit;
} __attribute__ ((packed)) rs232_param;
void get_sys_clk(void);
void uart_init(void);
void rs232_data_parse(u8 *data);

#endif
