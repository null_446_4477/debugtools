#ifndef __RS485DEV_H
#define __RS485DEV_H
#include "sys.h"

#define EN_USART2_RX 	1			//0,不接收;1,接收.

struct _rs485_param {
	u32 baud;
	u8 stop_bit;
	u8 data_bit;
	u8 parity_bit;
} __attribute__ ((packed)); 
typedef struct _rs485_param rs485_param;

void rs485_init(void);
void rs485_data_parse(u8 *data);

#endif
