#ifndef CAN_H
#define CAN_H

#define MAX_CAN_BUF_SIZE 4096
typedef struct _can_param {
	u8 baud;
	u8 filt_num;
	u32 filter;
	u32 mask;
} __attribute__ ((packed)) can_param;
struct can_frame {
	u32 id;
	u8 ide;
	u8 rtr;
	u8 dlc;
	u8 data[8];
};

void CANBUS_Config(void);
void can_interrupt(CAN_TypeDef * can, FunctionalState stat);
void can_data_parse(u8 *data);

#endif	   














