#include "usbd_cdc_vcp.h" 
#include "string.h"	
#include "stdarg.h"		 
#include "stdio.h"	 
#include "kfifo.h"
#include "sys_defines.h"
//////////////////////////////////////////////////////////////////////////////////	 
//本程序只供学习使用，未经作者许可，不得用于其它任何用途
//ALIENTEK STM32开发板
//usb vcp驱动代码	   
//正点原子@ALIENTEK
//技术论坛:www.openedv.com
//创建日期:2016/2/24
//版本：V1.0
//版权所有，盗版必究。
//Copyright(C) 广州市星翼电子科技有限公司 2009-2019
//All rights reserved									  
////////////////////////////////////////////////////////////////////////////////// 


//USB虚拟串口相关配置参数
LINE_CODING linecoding =
{
	115200,		//波特率
	0x00,   	//停止位,默认1位
	0x00,   	//校验位,默认无
	0x08    	//数据位,默认8位
}; 
//接收状态
//bit15，	接收完成标志
//bit14，	接收到0x0d
//bit13~0，	接收到的有效字节数目
u16 USB_USART_RX_STA=0;       				//接收状态标记	 

extern vu8 bDeviceState;			//USB连接 情况

struct kfifo usb_tx_fifo;
uint8_t usb_sendto_pc_fifo[USB_PC_SEND_LEN]; 
struct kfifo usb_rx_fifo;
uint8_t usb_recvform_pc_fifo[PC_USB_REC_LEN];

//虚拟串口配置函数(供USB内核调用)
CDC_IF_Prop_TypeDef VCP_fops = 
{
	VCP_Init,
	VCP_DeInit,
	VCP_Ctrl,
	VCP_DataTx,
	VCP_DataRx
}; 

//初始化fifo
void VCP_fifo_Init(void)
{
	//初始化USB发送FIFO
	kfifo_init(&usb_tx_fifo, usb_sendto_pc_fifo, USB_PC_SEND_LEN);
	//初始化USB接收FIFO
	kfifo_init(&usb_rx_fifo, usb_recvform_pc_fifo, PC_USB_REC_LEN);
} 
//初始化VCP
//返回值:USBD_OK
uint16_t VCP_Init(void)
{
	return USBD_OK;
} 
//复位VCP
//返回值:USBD_OK
uint16_t VCP_DeInit(void)
{ 
	return USBD_OK;
} 
//控制VCP的设置
//buf:命令数据缓冲区/参数保存缓冲区
//len:数据长度
//返回值:USBD_OK
uint16_t VCP_Ctrl (uint32_t Cmd, uint8_t* Buf, uint32_t Len)
{ 
	switch (Cmd)
	{
		case SEND_ENCAPSULATED_COMMAND:break;   
		case GET_ENCAPSULATED_RESPONSE:break;  
		case SET_COMM_FEATURE:break;  
		case GET_COMM_FEATURE:break;  
 		case CLEAR_COMM_FEATURE:break;  
		case SET_LINE_CODING:
			linecoding.bitrate = (uint32_t)(Buf[0] | (Buf[1] << 8) | (Buf[2] << 16) | (Buf[3] << 24));
			linecoding.format = Buf[4];
			linecoding.paritytype = Buf[5];
			linecoding.datatype = Buf[6]; 
			//打印配置参数
			printf("linecoding.format:%d\r\n",linecoding.format);
			printf("linecoding.paritytype:%d\r\n",linecoding.paritytype);
			printf("linecoding.datatype:%d\r\n",linecoding.datatype);
			printf("linecoding.bitrate:%d\r\n",linecoding.bitrate);
			break; 
		case GET_LINE_CODING:
			Buf[0] = (uint8_t)(linecoding.bitrate);
			Buf[1] = (uint8_t)(linecoding.bitrate >> 8);
			Buf[2] = (uint8_t)(linecoding.bitrate >> 16);
			Buf[3] = (uint8_t)(linecoding.bitrate >> 24);
			Buf[4] = linecoding.format;
			Buf[5] = linecoding.paritytype;
			Buf[6] = linecoding.datatype; 
			break; 
		case SET_CONTROL_LINE_STATE:break;   
		case SEND_BREAK:break;   
		default:break;  
	} 
	return USBD_OK;
}
//发送一个字节给虚拟串口(发给电脑)
//data:要发送的数据
//返回值:USBD_OK
uint16_t VCP_DataTx (uint8_t *data, uint16_t n)
{  
	if (n > kfifo_avail(&usb_tx_fifo)) {
		return USBD_BUSY;
	}
	kfifo_put(&usb_tx_fifo, data, n);
	return USBD_OK;
}

int VCP_Buffer_avail()
{
	return kfifo_avail(&usb_tx_fifo);
}

//处理从USB虚拟串口接收到的数据
//databuffer:数据缓存区
//Nb_bytes:接收到的字节数.
//返回值:USBD_OK
uint16_t VCP_DataRx (uint8_t* Buf, uint32_t Len)
{
	u16 length;
	if (Len >= PC_USB_REC_LEN) {
		return USBD_FAIL;
	}
	length = (u16)((Buf[LENGTH]<<8) + Buf[LENGTH + 1]);
	if (Buf[length - 1] != USB_TAIL) {
		return USBD_FAIL;
	}
	kfifo_put(&usb_rx_fifo, Buf, length);//放入fifo
	return USBD_OK;
}
//USB连接状态
u8 usb_connect_state(void) 
{
	static u8 usbstatus=0;	
	if(usbstatus!=bDeviceState)//USB连接状态发生了改变.
	{
		usbstatus=bDeviceState;//记录新的状态
		if(usbstatus==1)
		{
			//提示USB连接成功
			HAL_GPIO_WritePin(GPIOC, GPIO_PIN_2, GPIO_PIN_SET);//红色LED关
			HAL_GPIO_WritePin(GPIOC, GPIO_PIN_3, GPIO_PIN_RESET);//绿色LED开
		}else
		{
			//提示USB断开
			HAL_GPIO_WritePin(GPIOC, GPIO_PIN_2, GPIO_PIN_RESET);//红色LED开
			HAL_GPIO_WritePin(GPIOC, GPIO_PIN_3, GPIO_PIN_SET);//绿色LED关
		}
	}
	return bDeviceState;
}
